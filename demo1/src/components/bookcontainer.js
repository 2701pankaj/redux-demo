import React from "react";
import buyBook from '../redux/book/bookAction'
import {connect} from 'react-redux'
function bookContainer(props){
    return(
        <div>
            <h1>  Number of books  {props.numberOfBooks}</h1>
            <button onClick={props.buyBook}> Buy Book </button>

        </div>
    )
    
}


const mapStatetoProps=(state)=>{
    return {
        numberOfBooks:state.numberOfBooks
    }

}
const mapDispatchtoProps=(dispatch)=>{
    return{
        buyBook:function(){
            dispatch(buyBook());

        }
    }
}

export default connect(mapStatetoProps, mapDispatchtoProps)(bookContainer)